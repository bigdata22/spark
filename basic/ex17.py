## Broadcast Variable -> read only variable cached on each machine -> kept at all the worker nodes
## Broadcast variable must be loaded from local

from pyspark import SparkConf, SparkContext

def get_prefix(line):
    postcode = line.split(",")[4]
    return None if not postcode else postcode.split(" ")[0]

if __name__ == "__main__":
    conf = SparkConf().setAppName("ex17").setMaster("local[*]")
    sc = SparkContext(conf=conf)

    postcodes = open('uk-postcode.csv').read().split('\n')
    postcodes = [line.split(",") for line in postcodes if line != ""] 
    postcode_region = {line[0]: line[7] for line in postcodes if line[0] != "Postcode"}

    broadcasted_postcode_region = sc.broadcast(postcode_region)

    makerspaces = sc.textFile("makerspaces.CSV")
    regions = makerspaces.filter(lambda line: line.split(",")[0] != "Timestamp")\
                         .filter(lambda line: get_prefix(line) is not None)\
                         .map(lambda line: broadcasted_postcode_region.value[get_prefix(line)]\
                                if get_prefix(line) in broadcasted_postcode_region.value else "Unknow")

    regionsPairRDD = regions.map(lambda region: (region, 1))
    region_count =regionsPairRDD.reduceByKey(lambda x, y: x + y)
    sorted_region_count = region_count.sortBy(lambda pair: pair[1])

    for region, count in sorted_region_count.collect():
        print("{}: {}".format(region, count))

