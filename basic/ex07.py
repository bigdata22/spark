# Persist -> if you would like to take action on the rdd multiple times
# you can ask spark to hold your rdd in memory rdd.persist(StorageLevel.MEMORY_ONLY)


from pyspark import SparkConf, SparkContext, StorageLevel

if __name__ == "__main__":

    conf = SparkConf().setAppName("ex07").setMaster("local[*]")
    sc = SparkContext(conf=conf)

    integers = sc.parallelize(list(range(1, 10)))

    integers.persist(StorageLevel.MEMORY_ONLY) # = integers.cache()

    print(integers.count())
    print(integers.reduce(lambda x, y: x + y))