# sample (withReplacement, fraction, seed) -> chose some data from rdd randomly
# withReplacement = can elements be sample multiple times
# fraction = size of sample

# distinct -> very expensive -> shuffle all data aross the data to ensure that we receive 
# one copy of each element

# A.union(B) -> will contain duplicate if data exist in A an B

# intersection -> remove duplicate -> very expensive

# subtract -> very expensive

# cartesian -> (a, b) * (c, d) -> {(a, c),(a, d),(b, c),(b, d)} -> 
# very handy for similarity between all pairs


from pyspark import SparkConf, SparkContext

def removeHeader(line):
    return not (line.startswith("host"))

if __name__ == "__main__":

    conf = SparkConf().setAppName("ex04").setMaster("local[*]")
    sc = SparkContext(conf=conf)

    log_01 = sc.textFile("NASA_1~1.TSV")
    log_02 = sc.textFile("NASA_1~2.TSV")

    log_03 = log_01.union(log_02)
    cleanlog = log_03.filter(removeHeader)
    sample = cleanlog.sample(withReplacement=False, fraction=.1)

    sample.saveAsTextFile("out/Nasa.csv")


