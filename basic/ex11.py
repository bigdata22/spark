# efficent wordcount by reduceByKey (Transformation)

from pyspark import SparkConf, SparkContext

if __name__=="__main__":

    conf = SparkConf().setAppName("ex11").setMaster("local[*]")
    sc = SparkContext(conf=conf)

    lines = sc.textFile("word_count.text")
    wordRDD = lines.flatMap(lambda line: line.split(" "))
    wordPairRDD = wordRDD.map(lambda word: (word, 1))
    wordcount = wordPairRDD.reduceByKey(lambda x, y: x + y)

    for word, count in wordcount.collect():
        print("{} : {}".format(word, count))
