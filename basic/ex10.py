# Transformation on pairRDD -> mapValues -> you want to just form the values

from pyspark import SparkConf, SparkContext

if __name__=="__main__":

    conf = SparkConf().setAppName("ex10").setMaster("local[*]")
    sc = SparkContext(conf=conf)

    airportsRDD = sc.textFile("airports.text")
    airportsPairRDD = airportsRDD.map(lambda line: (line.split(",")[1], line.split(",")[3]))

    airportsUppercase = airportsPairRDD.mapValues(lambda value: value.upper())
    airportsUppercase.coalesce(1).saveAsTextFile("out/airportUppercase")

