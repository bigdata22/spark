# Transformation on pairRDD -> filter

from pyspark import SparkConf, SparkContext

if __name__=="__main__":

    conf = SparkConf().setAppName("ex09").setMaster("local[*]")
    sc = SparkContext(conf=conf)

    airportsRDD = sc.textFile("airports.text")
    airportsPairRDD = airportsRDD.map(lambda line: (line.split(",")[1], line.split(",")[3]))

    airportsNotInUSA = airportsPairRDD.filter(lambda keyValue: keyValue[1] != "\"United States\"")
    airportsNotInUSA.coalesce(1).saveAsTextFile("out/airportNotInUSA")