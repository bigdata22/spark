# Transformation -> reduceByKey, sortByKey

from pyspark import SparkConf, SparkContext
from AvgCount import AvgCount

if __name__ == "__main__":

    conf = SparkConf().setAppName("ex12").setMaster("local[*]")
    sc = SparkContext(conf=conf)

    lines = sc.textFile("RealEstate.csv")
    cleanedLines = lines.filter(lambda line: "Bedrooms" not in line)
    # or cleanedLines = lines.filter(lambda line: line.split(",")[3] != "Bedrooms")

    housePricePairRDD = cleanedLines.map(
        lambda line: (int(line.split(",")[3]), AvgCount(1, float(line.split(",")[2])))
        )

    housePriceTotal = housePricePairRDD.reduceByKey(
        lambda x, y: AvgCount(x.count + y.count, x.price + y.price)
        )

    for bedrooms, avgCount in housePriceTotal.collect():
        print("{} : ({}, {})".format(bedrooms, avgCount.count, avgCount.price))

    housePriceAvg = housePriceTotal.mapValues(lambda value: value.price / value.count)

    sortedhousePriceAvg = housePriceAvg.sortByKey(ascending=False)
    
    for bedrooms, avg in sortedhousePriceAvg.collect():
        print("({} : {})".format(bedrooms, avg))