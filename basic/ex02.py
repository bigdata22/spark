from pyspark import SparkContext, SparkConf

def splitComma(line):
    splits = line.split(",")
    return "{}, {}".format(splits[1], splits[6])

def filteredLatitude(line):
    splits = line.split(",")
    try:
        if float(splits[6]) > 40:
            return True
        return False
    except:
        return False

if __name__ == "__main__":
    conf = SparkConf().setAppName("ex02").setMaster("local[*]")
    sc = SparkContext(conf = conf)

    airports = sc.textFile("airports.text")
    airportsFilteredLatitude = airports.filter(filteredLatitude)
    airportsNameAndLatitude = airportsFilteredLatitude.map(splitComma)

    airportsNameAndLatitude.saveAsTextFile("out/airportLatitude")