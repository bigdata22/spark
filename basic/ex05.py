# Actions
# collect => return value or regular collection to the driver program
# => should not use in LARGE DATASETs

# count() => count all elements inside the rdd (8)
# countByValue() => count on elements group by each value {a:1, b:2, d:1, e:2, g:1, h:1}

# take(n) => take n top elements

# saveAsTextFile('addr')

# reduce

from pyspark import SparkConf, SparkContext

if __name__ == "__main__":

    conf = SparkConf().setAppName("ex05").setMaster("local[*]")
    sc = SparkContext(conf=conf)

    words = ['a', 'b', 'b', 'd', 'e', 'e', 'g', 'h']

    wordsRDD = sc.parallelize(words)

    words = wordsRDD.collect()
    count = wordsRDD.count()
    countByValue = wordsRDD.countByValue()
    print(count)
    print(countByValue)
    print(wordsRDD.take(3))

    for word in words:
        print(word)

