# coalesce only can decrease number of partition (optimized)
# repartition can both decrease and increase number of partition (is not optimized)

from pyspark import SparkConf, SparkContext

if __name__=="__main__":

    conf = SparkConf().setAppName("ex01").setMaster("local[*]")
    sc = SparkContext(conf=conf)

    rdd = sc.parallelize(list(range(1, 10)), 3)
    print(rdd.getNumPartitions())

    rdd.coalesce(1).saveAsTextFile("out/force_one_partition")

    rdd.repartition(5).saveAsTextFile("out/force_five_partition")