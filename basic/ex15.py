# Join operation => expensive
# Best Practice => Avoiding Shuffle => Use PartitionBy and Portable_hash

from pyspark import SparkConf, SparkContext
from pyspark.rdd import portable_hash

if __name__ == "__main__":
    conf = SparkConf().setAppName("ex15").setMaster("local")
    sc = SparkContext(conf=conf)

    ages = sc.parallelize([("Tom", 30), ("John", 20)])
    addresses = sc.parallelize([("James", "USA"), ("John", "UK")])

    ages.partitionBy(20, partitionFunc = portable_hash)
    addresses.partitionBy(20, partitionFunc= portable_hash)

    join = ages.join(addresses)
    join.saveAsTextFile("out/age_address_join")

    leftOuterJoin = ages.leftOuterJoin(addresses)
    leftOuterJoin.saveAsTextFile("out/age_address_leftOuterJoin")

    rightOuterJoin = ages.rightOuterJoin(addresses)
    rightOuterJoin.saveAsTextFile("out/age_address_rightOuterJoin")

    fullOuterJoin = ages.fullOuterJoin(addresses)
    fullOuterJoin.saveAsTextFile("out/age_address_fullOuterJoin")