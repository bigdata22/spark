# transformation => sortBy(function) => sortBy what?!!

from pyspark import SparkConf, SparkContext

if __name__ == "__main__":
    conf = SparkConf().setAppName("ex14").setMaster("local[*]")
    sc = SparkContext(conf=conf)

    lines = sc.textFile("word_count.text")
    words = lines.flatMap(lambda line: line.split(" "))
    wordPairRDD = words.map(lambda word: (word, 1))
    wordcountPerKey = wordPairRDD.reduceByKey(lambda x, y: x + y)

    sortedWordCount = wordcountPerKey.sortBy(lambda x: x[1], ascending=False)

    for key, value in sortedWordCount.collect():
        print(key, value)