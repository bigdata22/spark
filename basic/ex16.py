## Accumulator

from pyspark import SparkConf, SparkContext

if __name__ == "__main__":
    conf = SparkConf().setAppName("ex16").setMaster("local[*]")
    sc = SparkContext(conf=conf)

    total = sc.accumulator(0)
    missingSalaryPoints = sc.accumulator(0)

    def filterResponseFromCanada(response):
        splits = response.split(",")
        total.add(1)
        if not splits[14]:
            missingSalaryPoints.add(1)
        return splits[2] == "Canada"
        
    responseRDD = sc.textFile("stackoverflow.CSV")
    responseFromCanada = responseRDD.filter(filterResponseFromCanada)

    print("Count of responses from Canada: {}".format(responseFromCanada.count()))
    print("Total count of responses: {}".format(total.value))
    print("Count of responses missing salary: {}".format(missingSalaryPoints.value))