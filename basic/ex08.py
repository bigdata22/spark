## PairRDD -> can store key value pair

## PairRDD is created by list of tuples

## The coalesce method reduces the number of partitions in a DataFrame

from pyspark import SparkConf, SparkContext

if __name__ == "__main__":

    conf = SparkConf().setAppName("ex08").setMaster("local[*]")
    sc = SparkContext(conf=conf)

    tuples = [("Lily", 23), ("Matin", 15), ("Ahmad", 34), ("Elham", 1000)]
    pairRDD = sc.parallelize(tuples)

    pairRDD.coalesce(1).saveAsTextFile("out/PairRDD")

    # Change regular RDD to pair RDD 

    inputStrings = ["Lily 23", "Matin 15", "Ahmad 34", "Elham 1000"]
    regularRDD = sc.parallelize(inputStrings)
    pairRDD = regularRDD.map(lambda s: (s.split(" ")[0], s.split(" ")[1]))
    


