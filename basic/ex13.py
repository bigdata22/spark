# Transformation -> groupByKey

from pyspark import SparkConf, SparkContext

if __name__ == "__main__":

    conf = SparkConf().setAppName("ex13").setMaster("local[*]")
    sc = SparkContext(conf=conf)

    lines = sc.textFile("airports.text")
    countryAndAirportNamesPairRDD = lines.map(
        lambda line: (line.split(",")[3], line.split(",")[1])
    )

    airportsByCountry = countryAndAirportNamesPairRDD.groupByKey()

    for key, values in airportsByCountry.collect():
        for value in values:
            print(key, value)