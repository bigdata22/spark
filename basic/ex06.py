from pyspark import SparkConf, SparkContext

def stripElements(line):
    elements = line.split('\t')
    nelements = []
    for element in elements:
        if element != '':
            element = int(element.strip())
            nelements.append(element)

    return nelements

if __name__ == "__main__":

    conf = SparkConf().setAppName("ex06").setMaster("local[*]")
    sc = SparkContext(conf=conf)

    primenums = sc.textFile("prime_nums.text")

    p01 = primenums.flatMap(stripElements)

    #p01.filter(lambda number: number) filter empty string
    #p01.map(lambda number: int(number))

    p02 = p01.reduce(lambda x, y: x+y)
    print(p02)