# Create RDD
# rdd = sc.parallelize(list(range(1, 10))) Not practical for large datasets (Load Data On Program Driver)
# rdd = sc.textFile('addr') # From a distibuted file storage such as AS3 or HDFS

# Transformation functions
# Filter -> form RDD or remove some rows from RDD
# Map -> pass each element from RDD to map function -> for example calculate root square error of each row

# local = run on 1 core, local[2] run on 2 cores, local[*] = run on available cores

from pyspark import SparkConf, SparkContext

def splitComma(line):
    splits = line.split(",")
    return "{}, {}".format(splits[1], splits[2])

if __name__=="__main__":

    conf = SparkConf().setAppName("ex01").setMaster("local[*]")
    sc = SparkContext(conf=conf)

    airports = sc.textFile("airports.text")
    airportsInUS = airports.filter(lambda line: line.split(",")[3] == "\"United States\"")
    airportNameAndCityNames = airportsInUS.map(splitComma)
    airportNameAndCityNames.saveAsTextFile("out/airportInUsa_collect")

# spark-submit ex01.py
