# flatMap() vs map()

#flatMap() => mapping and then flattening [, , , , , , , , , ...]
#map() => mapping [[, , ,], [, ,], [], ...] countByValue dose not work on this function

from pyspark import SparkConf, SparkContext

if __name__=="__main__":

    conf = SparkConf().setAppName("ex03").setMaster("local[*]")
    sc = SparkContext(conf=conf)

    lines = sc.textFile("word_count.text")
    words = lines.flatMap(lambda line: line.split(" "))
    counts = words.countByValue()

    print(counts)
